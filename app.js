var mysql = require('mysql');
var express = require('express');
var nunjucks = require('nunjucks');

// Base de datos
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'endika',
  password : 'campus2b',
  database : 'aplicacion'
});
connection.connect();

const session = require('express-session');

// Express
var app = express();
app.use(session({
  secret: 'mysecret',
  resave: false,
  saveUninitialized: true
}));


app.use(express.urlencoded({ extended: true }))
var myLogger = function (req, res, next) {
  console.log('LOGGED');
  next();
};

app.use(myLogger);

// Servir archivos estáticos
app.use("/static", express.static('static'));

// Nunjuckss
nunjucks.configure('Views', {
    autoescape: true,
    express: app
});

app.get('/login', (req, res) => {
  res.render("login.html");
});

app.get('/', (req, res) => {
  if (!req.session.user) {
    res.redirect('/login');
    return;
  }
});

// Login
app.post('/login', (req, res) => {
  const username = req.body.usuario;
  const password = req.body.password;
  console.log(username);
  
  connection.query('SELECT * FROM usuarios WHERE usuario = ?', username, (err, results) => {
    if (err) {
      console.log(err);
      res.status(500).send('Error al intentar iniciar sesión');
      return;
    }
    console.log(results);
    if (results.length === 0) {
      res.status(401).send('Nombre de usuario incorrecto');
      return;
    }
    console.log(results);
    const user = results[0];
    console.log(password);
    console.log(user.pass);
    if (user.pass !== password) {
      res.status(401).send('Nombre de usuario o contraseña incorrectos');
      return;
    }
    else{
      console.log(results);
    req.session.user = username;
    res.status(200).render('exitoso.html');
    }
    
  });
});

//Logout
app.get('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.log(err);
      res.render('login.html', { ha_cerrado_sesion: true });
    }
  });
});


app.post('/data', function(req, res){
  var username=req.body.name;
  connection.query("INSERT INTO `names` (name) VALUES (?)", username.toString(), function(err, result){
      if(err) throw err;
          console.log("1 record inserted");
      });
  res.send(username);
});

app.get('/registro', (req, res) => {
  res.render("registro.html");
});

app.post('/registro', function(req, res){
  const username = req.body.usuario;
  const password = req.body.password;
  const password_confirmation = req.body.password_confirmation;
  const borndate = req.body.fecha_nacimiento;
  const nationality = req.body.poblacion;
  console.log(req.body);
  console.log(username);
  console.log(password);
  console.log(borndate);
  console.log(nationality);

  if(password != password_confirmation){
    res.status(400).send("passwords don't match");
    } else {
      const values={usuario: username, pass: password, fecha_nacimiento: borndate, poblacion: nationality};
      const sql= 'INSERT INTO usuarios SET ?';

      let q = connection.query(sql, values, (err, result) =>{
        if(err) {
          console.log(err);
          res.status(500).send("no tira")
        } else {
          res.send ("Tu cuenta se ha creado correctamente.");
          }
      });
      console.log(q);
    } 
});


app.get('/quejas', (req, res) => {
  // consultar a la db
  connection.query('SELECT * FROM quejas', (err, results) => {
    console.log(results);
    res.render("quejas.html", {quejas: results});
  })
});

app.post('/quejas', (req, res) => {
  console.log(req.body);
  const nombre = req.body.nombre;
  const queja = req.body.queja
  const sql = `INSERT INTO quejas (titulo, mensaje) VALUES (?, ?)`;
  const values = [nombre, queja];
  connection.query(sql,values)
  {  res.render("respuesta.html");}})


  app.listen(3000);{
    console.log("NEO WAKE UP...")
};