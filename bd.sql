CREATE TABLE usuarios (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    usuario TEXT(25),
    pass CHAR (20),
    fecha_nacimiento DATETIME,
    poblacion TEXT
);

INSERT INTO usuarios VALUES (2,'Endika', '12345', '1997/07/01', 'País Vasco' );

CREATE TABLE quejas (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    titulo TEXT(25),
    mensaje TEXT (150)
);